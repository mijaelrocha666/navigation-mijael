import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AntDesign } from '@expo/vector-icons';

import { Entypo } from '@expo/vector-icons'; 

// import Navigation from './Navigation';

// import HomeScreen from "./screens/HomeScreen"
import HomeScreen from './screens/HomeScreen';
import StackScreen from "./screens/StackScreen"
import FlatListScreen from './screens/FlatListScreen';
import RutinasScreen from './screens/RutinasScreen'
import ListImageRutinas from './components/ListImageRutinas'
import ListRutinas from './components/ListRutinas';
import PdfScreen from './screens/PdfScreen';

const HomeStackNavigator = createBottomTabNavigator();
const Tab = createBottomTabNavigator();


function MyStack() {
  return (
    <HomeStackNavigator.Screen 
      initialRouteName="HomeScreen"
    >
      <HomeStackNavigator.Screen 
        name='HomeScreen'
        component={HomeScreen}
      />

      <HomeStackNavigator.Screen 
        name='StackScreen'
        component={StackScreen}
        options={{
          herderBackTitleVisible: false
        }}
      />

      <HomeStackNavigator.Screen 
        name='ListRutinas'
        component={ListRutinas}
        options={{
          herderBackTitleVisible: false
        }}
      />

      <HomeStackNavigator.Screen 
        name='ListImageRutinas'
        component={ListImageRutinas}
        options={{
          herderBackTitleVisible: false
        }}
      />

    </HomeStackNavigator.Screen>
  )
}


function MyTabs() {
  return (
    <Tab.Navigator
        initialRouteName="Home"
        screenOptions={{
            tabBarActiveTintColor: '#FFF843'
        }}
    >
        <Tab.Screen 
        // style={styles.container}
            name="Home" 
            component={HomeScreen} 
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color, size }) => (
                  <AntDesign name="home" color="#000" size={20} />
                ),
                tabBarBadge: '9',
                headerShown: false,
                tabBarActiveTintColor: '#FFF843'
            }}
        />

        <Tab.Screen 
            name="StackScreen" 
            component={StackScreen}
            options={{
              tabBarLabel: 'LogIn',
              tabBarIcon: ({ color, size }) => (
                <AntDesign name="login" size={20} color="#000" />
              ),
              headerShown: false
          }}
        />

        <Tab.Screen 
            name="FlatListScreen" 
            component={FlatListScreen}
            options={{
              tabBarLabel: 'Menu',
              tabBarIcon: ({ color, size }) => (
                <AntDesign name="menu fold" size={20} color="#000" />
              ),
              headerShown: false
          }}
        />

        <Tab.Screen 
            name="RutinasScreen" 
            component={RutinasScreen}
            options={{
              tabBarLabel: 'Rutinas PDF',
              tabBarIcon: ({ color, size }) => (
                <AntDesign name="switcher" size={20} color="" />
              ),
              headerShown: false
          }}
        />

        <Tab.Screen 
            name="PdfScreen" 
            component={PdfScreen}
            options={{
              tabBarLabel: 'PDF',
              tabBarIcon: ({ color, size }) => (
                <AntDesign name="pdffile1" size={20} color="#000" />
              ),
              headerShown: false
          }}
        />
    </Tab.Navigator>
  );
}

export default function App() {
  return (
      <NavigationContainer>
          <MyTabs />
          <MyStack />
      </NavigationContainer>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     textAlign: 'center',
//     backgroundColor: '#1C1B1B',
//     alignItems: 'center',
//     justifyContent: 'center'
//   },
// });