import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, StatusBar, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { TextInput } from "react-native-gesture-handler";


export default function StackScreen() {
    const COLORS = {
        white: '#FFFFFF',
        black: '#000',
        red: '#E90716',
      
      }

    const navigation = useNavigation();

    return (
        <View 
            style={{ 
                width: '100%' , 
                height: '100%' ,   
        }}
        >
{/* FONDO */}
            <ImageBackground
                style={ styles.fondo }
                source={require('../assets/enmascarar_grupo_7.png')}
                resizeMode='cover'
            >
                <StatusBar style= '100%' />
{/* LOGO */}
            <Image
                style={{ 
                    width: 200 , 
                    height: 94 ,   
                    // marginVertical: 280
                }}
                    source={require("../assets/grupo_7.png")}
            ></Image>

                <View style={styles.container}> 

                    <View 
                        style={ styles.login }
                    >
                        <View
                            style={{
                                marginVertical: 40
                            }}
                        >

                            <Text
                                style={{
                                    textAlign: 'left',
                                    fontSize: 16,
                                    color: COLORS.white,
                                }}
                            >
                                Nombre de Usuario
                            </Text>
                            <TextInput
                                style={styles.input}
                            />
                        </View>

                            <Text
                                style={{
                                    textAlign: 'left',
                                    fontSize: 16,
                                    color: COLORS.white,
                                }}
                            >
                                Password
                            </Text>
                            <TextInput
// PARA OCULTAR LA CONTR    ASEÑA
                                password={true}
                                secureTextEntry={true}

                                style={styles.input}
                            />
                    </View>

                    <TouchableOpacity
// BOTON DE INGRESAR
                    onPress={() => navigation.navigate("FlatListScreen")}
                        style={{
                            backgroundColor: "#FFF843",
                            padding: 5,
                            marginTop: '10%',
                            width: 300,
                            height: 60,
                            alignSelf: 'center',
                            borderRadius: 8,
                        }}
                    >
                        <Text
                            style={{
                                textAlign: 'center',
                                fontSize: 35,
                                color: COLORS.black,
                            }}
                        >
                            INGRESAR
                        </Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </View>
    )
  }



  const styles = StyleSheet.create({
    fondo: {
        justifyContent: 'center',
        width:'100%',
        height:'100%',
        alignItems: 'center',
    },
    container: {
        textAlign: 'center',
        backgroundColor: '#1C1B1B',
        justifyContent: 'center',
        alignItems: 'center',
    },
    login: {
        // justifyContent: 'left',
        // alignItems: 'left',
        // textAlign: 'left',
    },
    input: {
        fontSize: 16,
        borderRadius: 8,
        width: 300,
        height: 60,
        borderWidth: 1,
        borderColor: '#FFF843',
        color: '#fff',
    }
  });