import React from "react";
import { View, Text, SafeAreaView, StyleSheet, StatusBar } from "react-native";

import { WebView } from 'react-native-webview';

import { useNavigation } from "@react-navigation/native";

const PdfScreen = () => {
    return (
        <View
            style={styles.container}
        >
            <Text
                style={styles.titulo}
            >
                Rutinas
            </Text>
            <Text
                style={styles.subtitulo}
            >
                Pecho • Apertuara de máquina contractora
            </Text>

            <View>
                <WebView source={{ uri:"https://google.com" }} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1C1B1B',
        width: '100%',
        height: '100%',
    },
    titulo: {
        // fontWeight: 'Dosis',
        padding: '7%',
        fontSize: 51,
        color: '#fff'
    },
    subtitulo: {
        color: '#FFF843',
        padding: '7%',
        fontSize: 17,
        textAlign: 'left',
        marginVertical: -50
    }
})

export default PdfScreen