import React from "react";
import { View, Text, SafeAreaView, StyleSheet, StatusBar } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";

import ListImageRutinas from '../components/ListImageRutinas'

const RutinasScreen = () => {

    const rutinasImage = [
        {
            image: 'Apertura en máquina contadora',
            id: '001'
        },
        {
            image: 'Apertura en máquina contadora',
            id: '002'
        },
        {
            image: 'Apertura en máquina contadora',
            id: '003'
        },
        {
            image: 'Apertura en máquina contadora',
            id: '004'
        },
    ]

    return (
        <View
            style={styles.container}
        >
            <FlatList 
                numColumns={
                    2
                }
                data = { rutinasImage }
                keyExtractor = { (item) => item.id}
                renderItem = { ({ item }) => <ListImageRutinas item = { item } /> }
                ItemSeparatorComponent = { () => <View 
                    style = {{ 
                        marginVertical: 15, 
                    }} />}
                ListHeaderComponent = { () => <Text
                    style = {styles.titulo}>
                    Rutinas
                </Text>}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#1C1B1B',
      width: '100%',
      height: '100%',
    //   marginTop: StatusBar.currentHeight,
    },
    titulo: {
        // fontWeight: 'Dosis',
        padding: '7%',
        fontSize: 51,
        color: '#fff'
    }
});

export default RutinasScreen