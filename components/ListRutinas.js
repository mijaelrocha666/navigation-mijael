import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";



const ListRutinas = (item) => {
    const { nombre } = item
    // console.log(item);
    return (
        <View
            style={styles.container}
        >
            <TouchableOpacity
                onPress={() => navigation.navigate("RutinasScreen")}
                style={{
                    alignItems:'center',
                    justifyContent:'center',
                    textAlign: 'center',
                }}
            >
                <Text
                    style={styles.textList}
                > { item.item.nombre } </Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1C1B1B',
        flex: 1,
        marginBottom: 10,
        padding: 3,    
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textList: {
        // paddingVertical: '21pt',
        backgroundColor: '#262323',
        paddingTop:'15%',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 20,
        borderRadius: 8,
        width: 130,
        height: 69,
        color: '#FFF843',
    }
});

export default ListRutinas