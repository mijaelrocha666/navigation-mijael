import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";



const ListImage = (item) => {
    const { image } = item
     const COLORS = {
        white: '#FFFFFF',
        red: '#E90716',
      
      }
    // console.log(item);
    return (
        <View
            style={styles.container}
        >
            <TouchableOpacity
                style={{
                    alignItems:'center',
                    justifyContent:'center',
                    textAlign: 'center',
                }}
                onPress={() => navigation.navigate("SettingsScreen")}
            >
                <View
                    style={{
                        alignContent: 'center',
                        alignItems: 'center',

                    }}
                >
                    <Image
                        style={styles.image}
                        source={require("../assets/pecho_ejemplo.png")}
                    >
                    
                    </Image>
                    <Text
                        style={{
                            color: COLORS.white,
                            textAlign: 'left'
                        }}
                    >
                        Apertura en máquina contractora
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1C1B1B',
        flex: 1,
        marginBottom: '2%',
        // padding: '10pt',    
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30,
    },
    image: {
        width: 150, 
        height: 220,
        borderRadius: 8,
    },
});

export default ListImage